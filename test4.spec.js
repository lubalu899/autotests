const { test, expect } = require('@playwright/test');

test.describe('Тестирование таблицы Денежных операций', () => 
{
    test.beforeEach(async ({ page }) =>       //Вход в систему, выполняется до всех тестов
    {      
        test.setTimeout(50000);
        await page.goto('http://localhost:4002/login');       // https://sadmin-dev.webslot.co/login
        await page.fill('input[name="username"]', 'elizaveta');
        await page.fill('input[name="password"]', 'user');
        await page.click('#submit_btn');
        await page.waitForLoadState();
        await expect(page.locator('.content-header-title')).toHaveText('Elizaveta'); 
    });


    test('Проверка таблицы денежных операций', async ({ page }) =>  {
        await page.click('text=Денежные операции');
        await page.waitForLoadState();
        await expect(page.locator('body h2')).toHaveText('Денежные операции');  //Заголовок
        await page.click('#cashin-cashout span');       //Клик на ручное задание времени
        await page.locator('#cashin-cashout-from').fill('22.04.2022 00:00');    //Начальная дата
        await page.locator('#cashin-cashout-to').fill('23.05.2022 00:00');      //Конечная дата
        await page.click('text=Обновить');
        await page.waitForLoadState();

        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div:nth-child(2) > div > div > span > small');       //Клик на включая дочерних (по умолчанию вкл, мы выключаем) 
        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div:nth-child(2) > div > span > span.selection > span > ul');    //Клик на выпор партнера
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#cashin-cashout-tab > div > div')).toHaveText('Нет данных'); 

        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div:nth-child(2) > div > div > span > small');       //Клик на включая дочерних
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#cashin-cashout-table > div > div > ul > li:nth-child(2) > a')).toHaveText('2');         //2-я страница

        await page.locator('#cashin-cashout > div > div > div.row.mb-1 > div:nth-child(3) > div > div > input').fill('19541');      //Вставить ИД игрока = 19541
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#cashin-cashout-table tr > td:nth-child(5)')).toHaveText('651936');

        await page.locator('#cashin-cashout > div > div > div.row.mb-1 > div:nth-child(3) > div > div > input').fill(' ');  //Удалить ИД
        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div.col-md-4 > span > span.selection > span > ul');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#cashin-cashout-table tr:nth-child(20) > td:nth-child(1)')).toHaveText('3253');

        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div.col-md-4 > span > span.selection > span > ul > li.select2-selection__choice > span');
        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div.col-md-4 > span > span.selection > span > ul');
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#cashin-cashout-table tr:nth-child(1) > td:nth-child(3)')).toHaveText('-4430.00');
        await expect(page.locator('#cashin-cashout-table > div > table > tbody > tr:nth-child(2) > td:nth-child(3)')).toHaveText('-500.00');

        await page.click('#cashin-cashout > div > div > div.row.mb-1 > div.col-md-4 > span > span.selection > span > ul');      //Клик 
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#cashin-cashout-table tr:nth-child(19) > td:nth-child(5)')).toHaveText('536951(ttt)');
        await expect(page.locator('#cashin-cashout > div > div > div.row.mb-1 > div.col-md-4 > span > span.selection > span')).toHaveText(/player_in/);
        await expect(page.locator('#cashin-cashout > div > div > div.row.mb-1 > div.col-md-4 > span > span.selection > span')).toHaveText(/player_out/);
    });

    
});
