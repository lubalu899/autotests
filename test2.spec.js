const { test, expect } = require('@playwright/test');

test.describe('Тестирование таблицы Промоакций', () => 
{
    test.beforeEach(async ({ page }) =>       //Вход в систему, выполняется до всех тестов
    {      
        test.setTimeout(50000);
        await page.goto('http://localhost:4002/login');       // https://sadmin-dev.webslot.co/login
        await page.fill('input[name="username"]', 'elizaveta');
        await page.fill('input[name="password"]', 'user');
        await page.click('#submit_btn');
        await page.waitForLoadState();
        await expect(page.locator('.content-header-title')).toHaveText('Elizaveta'); 
    });

    test('Таблица промоакций', async ({ page }) =>  {
        await expect(page.locator('#main-menu-navigation > li:nth-child(10) > a')).toHaveText('Промоакции'); 
        await page.click('text=Промоакции');           //Переход на страницу Промоакций
        await page.waitForLoadState();
        await expect(page.locator('body div.content-header-left.col-md-4.col-xs-12 > h2')).toHaveText('Промоакции'); 
        
        await page.click('#manual-time-set-promo');                             //Ручное задание времени
        await page.locator('#promotion-from').fill('22.04.2022 14:00');               //Начальная дата
        await page.locator('#promotion-to').fill('21.05.2022 00:00');                 //Конечная дата
        await page.click('text=Обновить');             //Обновить
        await page.waitForLoadState();

        await expect(page.locator('#promotions-tab table')).not.toContainText('407');          //Нет промоакции с ID=407 (из-за времени старта)
        await expect(page.locator('#promotions-tab table')).toHaveText(/Расчитана/); 
        await expect(page.locator('#promotions-tab table')).toHaveText(/Завершена/); 
        await expect(page.locator('#promotions-tab table')).toHaveText(/Активна/); 
        await expect(page.locator('#promotions-tab table')).toHaveText(/Не состоялась/); 

        await page.click('#promotions-tab div:nth-child(1) > div:nth-child(2) > span > span.selection > span > ul');           //Клик на поле выбрать Партнёра
        await page.keyboard.press('Enter');             //Выбрали партнёра Elizaveta
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab table')).not.toContainText('425');

        await page.click('#include_children');          //Клик на □ Включая дочерних 
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab table')).toHaveText(/425/);             //Есть у дочернего партнёра

        await page.locator('#promotions-tab div:nth-child(2) > div:nth-child(2) > div > input').fill('409');          //Фильтр по ID промоакции
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab td:nth-child(3)')).toHaveText('депозит1');

        await page.click('#promotions-tab div:nth-child(2) > div:nth-child(2) > div > input');         //Клик на фильтр ID
        await page.keyboard.press('Control+A');         //Стереть ID
        await page.keyboard.press('Backspace');
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await page.click('#promotions-tab div:nth-child(2) > div:nth-child(1) > div > select');        //Клик на выбор Статуса
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');             //Активные
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab tr:nth-child(1) > td:nth-child(1)')).toHaveText('428');                  //Есть в таблице по селектору 428 и 410
        await expect(page.locator('#promotions-tab tr:nth-child(2) > td:nth-child(1)')).toHaveText('410'); 

        await page.click('#promotions-tab div:nth-child(3) > div:nth-child(1) > div > select');             //Клик на Типы промоакций
        await page.keyboard.press('ArrowDown');         //Вниз
        await page.keyboard.press('Enter');             //Выбрать Бонус на депозит
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab div.top-border-table.mx-2 > p')).toHaveText('Нет данных'); 

        await page.click('#promotions-tab div:nth-child(2) > div:nth-child(1) > div > select');        //Клик на выбор Статуса
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');             //Выбрать Все
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await page.click('#promotions-tab div:nth-child(3) > div:nth-child(1) > div > select');             //Клик на Типы 
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');             //Выбрать Все
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await page.locator('#promotions-tab > div > div > div > div.card-body > div > div.row.mb-1 > div:nth-child(3) > div:nth-child(2) > div > input').fill('промоакция3');            //Поиск по Названию
        await page.click('text=Обновить');              //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab table')).toHaveText(/Elizaveta/); 
        await expect(page.locator('#promotions-tab table')).not.toContainText('WebSlot');

        await page.click('#promotions-tab button.invisible-btn.btn.btn-in-table.btn-icon.float-xs-right.btn-outline-danger.btn-xs');      //Клик Скрыть промоакцию
        await expect(page.locator('body > div.bootbox.modal.fade.bootbox-confirm.in > div > div')).toHaveText(/Подтверждение/); 
        await expect(page.locator('body > div.bootbox.modal.fade.bootbox-confirm.in > div > div > div.modal-body')).toHaveText('Промоакция будет скрыта от игроков. Вы уверены?'); 
        await page.locator('body > div.bootbox.modal.fade.bootbox-confirm.in > div > div > div.modal-footer > button.btn.btn-danger').click();
        await expect(page.locator('body > div.bootbox.modal.fade.success.in > div > div > div.modal-body')).toHaveText(/Успешно/); 
        await page.click('text=OK');
    });


    test('Создание промоакции', async ({ page }) =>  {
        await page.click('text=Промоакции');           //Переход на страницу Промоакций
        await page.waitForLoadState();
        
        await page.click('#create-promotion');          //Создать промоакцию
        await page.waitForLoadState();
        await expect(page.locator('#promotion-content > div.modal-header > h4')).toHaveText(/Создать промоакцию/); 

        await page.click('#promotion-content > div.modal-footer > button.btn.btn-success > span');          //Клик Создать (с пустыми значениями)
        await expect(page.locator('#create-promotion-form > div:nth-child(10) > div > label')).toHaveText(/Введите значение/); 
    
        await page.click('#create-promotion-form span > span:nth-child(1)');                //Elizaveta - партнер
        await page.click('#create-promotion-form > div:nth-child(4) > div:nth-child(6) > label > input[type=radio]');       //Процент при любом пополнении
        await page.click('#create-promotion-form > div:nth-child(5) > div:nth-child(2) > input[type=checkbox]');            //Клик снять одноразовую акцию
        await page.locator('#create-promotion-form > div:nth-child(6) > div:nth-child(2) > div:nth-child(2) > input').fill('10');           // Вейджеры
        await page.locator('#create-promotion-form > div:nth-child(7) > div:nth-child(2) > div > div > input').fill('4');   //Процент зачисления
        await page.locator('#create-promotion-form > div:nth-child(8) > div:nth-child(2) > div > div > input').fill('20')   //Мин. сумма бонуса
        await page.locator('#create-promotion-form > div:nth-child(9) > div:nth-child(2) > div > div > input').fill('500'); //Макс сумма ополнения
        await page.locator('#create-promotion-form > div:nth-child(10) > div > input').fill('2345');
        await page.locator('#create-promotion-form > div:nth-child(11) > div > input').fill('промоакция6');
        await page.locator('#create-promotion-form > div:nth-child(12) > div > textarea').fill('короткое описание промотест6');              //Кратое описание
        await page.locator('#create-promotion-form > div:nth-child(13) > div > div > div.ql-container.ql-snow > div.ql-editor.ql-blank').fill('промотест6 длинное описание');       //Описание
        await page.click('#promotion-content > div.modal-footer > button.btn.btn-success');             //Создать
        
        await page.waitForLoadState();
        await page.click('body > div.bootbox.modal.fade.success.in > div > div > div.modal-footer > button');             //OK
        await page.waitForLoadState();
        await page.click('#promotions-tab > div > div > div > div.card-body > div > div.row.mb-1 > div:nth-child(4) > button'); //Обновить
        await page.waitForLoadState();
        await expect(page.locator('#promotions-tab tr:nth-child(1) > td:nth-child(3)')).toHaveText('промоакция6'); 

    });
});
