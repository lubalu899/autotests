const { test, expect } = require('@playwright/test');


test.describe('Тестирование страницы партнёра-зал', () => 
{
  config = { timeout: 100000 }
  test.beforeEach(async ({ page }) =>       //Вход в систему и в зал, выполняется до всех тестов
  {      
    test.setTimeout(50000);
    await page.goto('http://localhost:4002/login');       // https://sadmin-dev.webslot.co/login
    await page.fill('input[name="username"]', 'elizaveta');
    await page.fill('input[name="password"]', 'user');
    await page.click('#submit_btn');
    await expect(page.locator('.content-header-title')).toHaveText('Elizaveta'); 

    await page.click('#\\32 03_anchor');        //Зал
    await page.waitForLoadState();
    await expect(page).toHaveURL(/.*203/);
    await expect(page.locator('.content-header-title')).toHaveText('liza_zal1'); 
    await page.waitForLoadState();
  });


  test('Добавление нового игрока', async ({ page }) => 
  {
    await page.click('#quick-add-player');      //Добавить пользователя
    await expect(page.locator('.bootbox')).toHaveText(/Будет создан новый игрок. Вы уверены?/); 
    await page.click('body > div.bootbox.modal.fade.in > div > div > div.modal-footer > button.btn.btn-success');   //OK
    await expect(page.locator('.bootbox')).toHaveText(/Это поле необходимо заполнить./); 
    
    await page.locator('.bootbox #sum').fill('50'); 
    await page.locator('.bootbox #comment').fill('com90');
    await page.click('body > div.bootbox.modal.fade.in > div > div > div.modal-footer > button.btn.btn-success');
    await expect(page.locator('body > div.bootbox.modal.fade.success.in > div > div > div.modal-header > h4')).toHaveText(/.*Игрок создан*./, { timeout: 3000 }); 
    await page.click('.bootbox button.btn.btn-success');
    await page.waitForLoadState();
  });


  test('Настройки на странице зала', async ({ page }) => 
  {
    await page.click('#quick-group-settings');          //Настройки группы
    await expect(page.locator('#group-settings-modal > div > div > div.modal-header > h4')).toHaveText(/Настройки группы/); 
    await page.click('#group-settings-form > div > div > ul > li:nth-child(2) > a');                                    //Оператор
    await page.click('#tab3 > div > div > div:nth-child(2) > div > div > label:nth-child(3) > input[type=radio]');      //Ежедневные
    await page.click('#tab3 > div > div > div:nth-child(5) > div > div > label > div > span > input[type=checkbox]');   //Возможность удалять игрока
    await page.click('#tab3 > div > div > div:nth-child(4) > div > div > label > div > span > input[type=checkbox]');   //Возможность редактировать игрока
    await page.click('#group-settings-modal > div > div > div.modal-footer > div > div:nth-child(3) > button.btn.btn-success');         // Сохранить
    await expect(page.locator('body > div.bootbox.modal.fade.success.in > div > div > div.modal-body > div')).toHaveText(/Настройки изменены/); 
    await page.click('body > div.bootbox.modal.fade.success.in > div > div > div.modal-footer > button');               //OK
  });


  test('Добавить пользователя', async ({ page }) => 
  {          
    await page.click('body > div.robust-content.content.container-fluid > div > div.content-header > div > div.col-md-12.col-lg-10 > div > div:nth-child(3) > button');   //Добавить пользователя
    await expect(page.locator('#create-user-modal > div > div > div.modal-header > h4')).toHaveText(/Добавить пользователя/);
    await page.locator('#create-user-modal #login').fill('testuser4');               //Заполняем поля пользователя
    await page.locator('#create-user-modal #email').fill('t4@email.com');
    await page.locator('#create-user-modal #phone').fill('+380171717170');
    
    await page.click('#create-user-modal #role');                                         //Выбрать роль
    await page.locator('#create-user-modal').click('#role > option:nth-child(1)');        //Менеджер (другие не выбираются почему-то, по разному пробовала)... теперь знаю как менять
    await page.click('#create-user-modal > div > div > div.modal-footer > button.btn.btn-success > span');      //Создать
    await expect(page.locator('body > div.bootbox.modal.fade.bootbox-alert.in > div > div > div.modal-header > h4')).toHaveText(/Пользователь создан/); 
    await page.click('body > div.bootbox.modal.fade.bootbox-alert.in > div > div > div.modal-footer > button');     //OK
  });


  test('Проверка таблицы Денежных операций', async ({ page }) =>              //Вывод денежных операций (Из БД, возможны изменения)
  {       
    await expect(page.locator('#cashin-cashout > div.card-header > div > div.col-md-4 > h4')).toHaveText(/Денежные операции/);
    
    await page.locator('#cashin-cashout-from').fill('01.04.2022');     //Ввести новую дату   
    await page.click('#report-get');                                   //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#cashin-cashout-tab > div > div')).toHaveText(/3277/);   //ID
    await expect(page.locator('#cashin-cashout-table > div > div > ul > li:nth-child(3) > a')).toHaveText(/3/);                         //Макс.страница

    await page.click('#cashin-cashout-collapse > div > div.row.mb-1 > div.col-md-5 > span > span.selection > span > ul');       //Тип транзакции
    await page.waitForLoadState();     
    await page.locator('body > span > span > span').click('#select2-transactions_type-result-st0k-player_out');         //player_out    (здесь в Firefox выбирает player_in и тест падает, на Chrome и Webkit всё ок)
    await page.click('#report-get');                                   //Обновить
    await expect(page.locator('#cashin-cashout-tab > div > div')).toHaveText(/3245/); 
    await expect(page.locator('#cashin-cashout-tab > div > div')).toHaveText(/3244/); 
    await expect(page.locator('#cashin-cashout-tab > div > div')).toHaveText(/3200/); 

    await page.click('#cashin-cashout-collapse > div > div.row.mb-1 > div.col-md-5 > span > span.selection > span > ul > li.select2-selection__choice > span');  //Убрать player_out 
    await page.click('#report-get');                                   //Обновить

    await page.locator('#cashin-cashout-collapse > div > div.row.mb-1 > div:nth-child(2) > div > div > input').fill('19620');       //Вставить ID игрока
    await page.click('#report-get');                                   //Обновить
    await expect(page.locator('#cashin-cashout-tab > div > div')).toHaveText(/3277/);            //ID денежной операции
  });


  test('Проверка таблицы Пользователей', async ({ page }) => 
  { 
    await expect(page.locator('#users > div.card-header.clearfix > div > div:nth-child(1) > h4')).toHaveText(/Пользователи/); 
    await expect(page.locator('#users-table')).toHaveText(/тест777/);     //В табл. Пользватели есть Логин = тест777

    //Теперь проверим добавление пользователя и его же удаление
    await page.click('#add-user');
    await expect(page.locator('#create-user-modal > div > div > div.modal-header > h4')).toHaveText(/Добавить пользователя/);
    await page.click('#create-user-modal > div > div > div.modal-footer > button.btn.btn-success > span');      //Создать
    await expect(page.locator('#login-error')).toHaveText(/Это поле необходимо заполнить./);                    //Ошибка при создании пустого пользователя
    await page.locator('#create-user-form #login').fill('addusr1');                                             //Здесь нужно каждый раз вводить новый логин, т.к. даже после удаления логин остаётся в БД
    await page.locator('#create-user-form #email').fill('ffff@email.com');
    await page.locator('#create-user-form #phone').fill('+380123456789');
    await page.click('#create-user-modal > div > div > div.modal-footer > button.btn.btn-success > span');              //Создать
    await page.waitForLoadState();

    await expect(page.locator('body > div.bootbox.modal.fade.bootbox-alert.in > div > div > div.modal-header > h4')).toHaveText(/Пользователь создан/);     //Табличка об успешном создании
    await page.click('body > div.bootbox.modal.fade.bootbox-alert.in > div > div > div.modal-footer > button');         //OK
    await page.waitForLoadState();
    await expect(page.locator('#users-table')).toHaveText(/addusr1/);                                                   //Меняем логин как у добавленного пользователя
    await page.click('#users-table > div > table > tbody > tr:nth-child(1) > td:nth-child(5) > button');                //Кнопка удалить пользователя (первого)
    await expect(page.locator('body > div.bootbox.modal.fade.bootbox-confirm.in > div > div > div.modal-body > div')).toHaveText(/Вы уверены?/);
    await page.click('body > div.bootbox.modal.fade.bootbox-confirm.in > div > div > div.modal-footer > button.btn.btn-danger');        //Удалить
    await page.waitForLoadState();
    await expect(page.locator('#users-table')).not.toContainText('addusr1');                                            //Здесь тоже меняем логин для проверки
  });


  test('Проверка таблицы Истоия лимит-банка', async ({ page }) => 
  {
    await expect(page.locator('body > div.robust-content.content.container-fluid > div > div.content-body > div:nth-child(2) > div.col-lg-8.col-md-12 > div.row > div > div > div.card-header.clearfix > div > div.col-md-4 > h4')).toHaveText(/История банк-лимита/); 
    await page.click('#bank-limit-from');
    await page.locator('#bank-limit-from').fill('01.04.2022');     //Ввести новую дату  
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-collapse > div')).toHaveText(/18.04.2022 16:11/);      //Одна из дат (самая последняя)

    await page.click('#add-balance');                              //Пополнить баланс, чтобы проверить, что он добавился в таблицу
    await page.locator('.bootbox #sum').fill('15');
    await page.click('body > div.bootbox.modal.fade.in > div > div > div.modal-footer > button.btn.btn-success');       //OK
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-table > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(4)')).toHaveText(/\+15,00/);

    await page.click('#reduce-balance');                           //Списать баланс
    await page.locator('.bootbox #sum').fill('14.99');
    await page.click('body > div.bootbox.modal.fade.in > div > div > div.modal-footer > button.btn.btn-success');       //OK
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-table > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(4)')).toHaveText(/\-14,99/);

    await page.click('#add-balance');                              //Проверка на пополнение отрицательного баланса
    await page.locator('.bootbox #sum').fill('-5.64');
    await page.click('body > div.bootbox.modal.fade.in > div > div > div.modal-footer > button.btn.btn-success');       //OK
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-table > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(4)')).toHaveText(/\-5,64/);

    //Проверка фильтрации колонок
    await page.click('#type_select');
    await page.keyboard.press('ArrowDown');                  //Кнопка стрелка вниз
    await page.keyboard.press('Enter');                      //Кнопка ввод
    await page.waitForLoadState();
    await expect(page.locator('#type_select')).toHaveText(/Пополнение/);      //Пополнение
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-table > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(4)')).toHaveText(/\+15,00/);     //проверка первого баланса
    
    await page.click('#type_select');
    await page.keyboard.press('ArrowDown');                  //Кнопка стрелка вниз
    await page.keyboard.press('Enter');                      //Кнопка ввод
    await page.waitForLoadState();
    await expect(page.locator('#type_select')).toHaveText(/Списание/);        //Списание
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-table > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(4)')).toHaveText(/\-5,64/);     //проверка первого баланса
  
    await page.click('#bank-limit-collapse > div > div.row > div:nth-child(2) > div > span > span.selection > span > ul');
    await page.keyboard.press('ArrowDown');                  //Кнопка стрелка вниз
    await page.keyboard.press('Enter');                      //Кнопка ввод
    await page.waitForLoadState();
    await expect(page.locator('#bank-limit-table > p')).toHaveText(/Нет данных/);

    await page.click('#bank-limit-collapse > div > div.row > div:nth-child(2) > div > span > span.selection > span > ul > li.select2-selection__choice > span');    //Закрыть liza_zal1
    await page.waitForLoadState();
    await page.click('#type_select');
    await page.keyboard.press('ArrowUp');                  //Кнопка стрелка вверх
    await page.keyboard.press('ArrowUp');                  //Кнопка стрелка вверх
    await page.keyboard.press('Enter');                      //Кнопка ввод
    await page.click('#bank-limit-tabs > li:nth-child(3) > a');     //Бонус
    await page.click('#bank-limit-refresh');                       //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#bonus-tab > div > div')).toHaveText(/\+700,00/);
  });


  test('Проверка таблицы Событий', async ({ page }) => 
  {
    await expect(page.locator('#new_events > div.card-header > div > div.col-md-4 > h4')).toHaveText(/События/);
    await page.click('#events-collapse > div > div:nth-child(1) > div.col-md-6 > div > div.input-group.float-xs-right.ml-1 > span');    //Ручное задание времени
    await page.locator('#info-stats-from').fill('18.05.2022 13:45:00');         //Ввести стартовую дату и время
    await page.locator('#info-stats-to').fill('18.05.2022 13:50:00');         //Ввести конечную дату и время
    await page.click('#info-stats-refresh');        //Обновить                   
    await page.waitForLoadState();
    await expect(page.locator('#bets > div')).toHaveText(/807106/);             //id = 807106
    await expect(page.locator('#bets > div')).toHaveText(/\+1.00/);             //Разница = +1.00

    await page.locator('#betsum-start').fill('2');  //Сумма ставки с 2
    await page.click('#info-stats-refresh');        //Обновить
    await page.waitForLoadState();                  //Подождать
    await expect(page.locator('#bets > div')).not.toContainText('+1.00');       //Отсутствует разница +1.00
    
    await page.locator('#resultsum-start').fill('50');      //Сумма выигрыша с 50
    await page.click('#info-stats-refresh');                //Обновить
    await expect(page.locator('#bets > div')).not.toContainText('+20.00');
    await expect(page.locator('#bets > div')).toHaveText(/\-50.00/);

    await page.click('#resultsum-start');
    await page.keyboard.press('Backspace');               //Стереть 50
    await page.keyboard.press('Backspace');
    await page.locator('#resultsum-end').fill('10');      //Сумма выигрыша до 10
    await page.click('#info-stats-refresh');                //Обновить
    await page.waitForLoadState(); 
    await expect(page.locator('#bets_table > div.table-responsive > table > tbody > tr:nth-child(6) > td:nth-child(5)')).toHaveText('+100.00');         //Есть +100
    await expect(page.locator('#bets > div')).not.toContainText(/\-50.00/);             //Нет -50

    await page.locator('#bet_id').fill('807102');           //Фильтр по id ставки
    await page.click('#info-stats-refresh');                //Обновить    
    await page.waitForLoadState(); 
    await expect(page.locator('#bets_table > div.table-responsive > table > tbody > tr > td:nth-child(2)')).toHaveText('18.05.2022 13:48:08');
  });

  
  test('Проверка таблицы Игроков', async ({ page }) => 
  {
    await expect(page.locator('#players > div.card-header > div > div.col-md-4 > h4')).toHaveText('Игроки');            //проверка что есть таблица Игроки   
    await page.locator('#players-collapse > div > div.row.mb-1 > div:nth-child(1) > div > input').fill('19507');        //ID = 19507
    await page.click('#players-tabs > li:nth-child(1) > a');              //Клик на Активные
    await page.waitForLoadState(); 
    await expect(page.locator('#players-table > div')).toHaveText(/380123456789/);     //логин игрока с ID = 19507
    
    await page.click('#players-collapse > div > div.row.mb-1 > div:nth-child(1) > div > input');          //Клик на поле с ID
    await page.keyboard.press('Control+A');               //Стереть 19507
    await page.keyboard.press('Backspace');
    await page.click('#players-tabs > li:nth-child(3) > a');          //Удаленные
    await expect(page.locator('#players-table > div')).toHaveText(/522663/);                    //Есть в таблице
    await expect(page.locator('#players-table > div')).not.toContainText('380123456789');       //Нет в таблице

    await page.click('#players-tabs > li:nth-child(1) > a');              //Клик на Активные
    await page.waitForLoadState(); 
    await page.click('#players-collapse > div > div.row.mb-2 > div.col-md-2 > div > label.mr-1');        //Клик на □ Баланс
    await page.click('#players > div.card-header > div > div.col-md-8 > button.btn.btn-outline-info.btn-outline-darken-2.float-xs-right');      //Обновить
    await page.waitForLoadState();
    await expect(page.locator('#players-table > div > table')).not.toContainText('19542');

    await page.locator('#players-collapse > div #comment').fill('com88');
    await page.waitForLoadState();
    await expect(page.locator('#players-table > div')).toHaveText(/19620/);                    //Есть в таблице
  });
});
