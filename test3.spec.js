const { test, expect } = require('@playwright/test');

test.describe('Тестирование таблицы SMS', () => 
{
    test.beforeEach(async ({ page }) =>       //Вход в систему, выполняется до всех тестов
    {      
        test.setTimeout(50000);
        await page.goto('http://localhost:4002/login');       // https://sadmin-dev.webslot.co/login
        await page.fill('input[name="username"]', 'elizaveta');
        await page.fill('input[name="password"]', 'user');
        await page.click('#submit_btn');
        await page.waitForLoadState();
        await expect(page.locator('.content-header-title')).toHaveText('Elizaveta'); 
    });

    test('Проверка таблицы SMS', async ({ page }) =>  {
        await page.click('text=SMS');       //Клик на СМС
        await page.waitForLoadState();
        await expect(page.locator('body h2')).toHaveText(/SMS/);            //Заголовок

        await page.click('#manual-time-set');       //Ручное задание времени
        await page.locator('#sms-from').fill('01.04.2022 01:00');       //Начальная дата
        await page.locator('#sms-to').fill('23.05.2022 15:00');         //Конечная дата
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses tr:nth-child(1) > td:nth-child(1)')).toHaveText('498');
        await expect(page.locator('#smses tr:nth-child(5) > td:nth-child(5)')).toHaveText('592672');

        await page.click('#status_type');           //Фильтр статуса = send
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();

        await expect(page.locator('#smses tr:nth-child(1) > td:nth-child(1)')).toHaveText('497');
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).not.toContainText('498');
        
        await page.click('#status_type');           //Фильтр статуса = fail
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).toHaveText(/498/);
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).not.toContainText('497');
    
        await page.click('#status_type');           //Фильтр статуса = wait
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses p')).toHaveText(/Нет данных/);

        await page.click('#status_type');           //Фильтр статуса = Bce
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');
        await page.click('#type');                  //Фильтр по Типу = direct
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div')).toHaveText(/495/);
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div')).not.toContainText('498');
        
        await page.click('#type');                  //Фильтр по Типу = recovery
        await page.keyboard.press('ArrowDown');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        //await expect(page.locator('#smses tr > td:nth-child(8)')).toHaveText('22.04.2022 15:49');     на этой строке глючит, когда писала код всё норм отрабатывал

        await page.click('#type');                  //Фильтр по Типу = registration
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses p')).toHaveText(/Нет данных/);

        await page.click('#type');                  //Фильтр по Типу = Bce
        await page.keyboard.press('ArrowUp');
        await page.keyboard.press('Enter');
        await page.click('#smses > div > div > div.row.mb-1 > div:nth-child(1) > div:nth-child(2) > span > span.selection > span > ul');
        await page.keyboard.press('Enter');         //Выбор партнера Elizaveta
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses p')).toHaveText(/Нет данных/);
        
        await page.click('#include_children');      //Клик на Включая партнёра
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses tr:nth-child(1) > td:nth-child(1)')).toHaveText('498');
        await expect(page.locator('#smses tr:nth-child(5) > td:nth-child(5)')).toHaveText('592672');

        await page.locator('#smses > div > div > div.row.mb-1 > div:nth-child(2) > div:nth-child(2) > div > input').fill('19092');      //Фильтр по ИД игрока
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div > table > tbody > tr > td:nth-child(5)')).toHaveText('592672');
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).not.toContainText('380123456789');
       
        await page.locator('#smses > div > div > div.row.mb-1 > div:nth-child(2) > div:nth-child(2) > div > input').fill(' ');
        await page.locator('#smses > div > div > div.row.mb-1 > div:nth-child(3) > div:nth-child(2) > div > input').fill('497');        //Фильтр по ИД сообщения
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses tr > td:nth-child(4)')).toHaveText('тест');
        await expect(page.locator('#smses tr > td:nth-child(1)')).toHaveText('497');

        await page.locator('#smses > div > div > div.row.mb-1 > div:nth-child(3) > div:nth-child(2) > div > input').fill(' ');
        await page.locator('#smses > div > div > div.row.mb-1 > div:nth-child(4) > div > div > input').fill('380123456789');
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses tr:nth-child(4) > td:nth-child(1)')).toHaveText('495');
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).not.toContainText('492');

        await page.locator('#sms-from').fill('22.04.2022 12:40');       //Проверим фильтрацию по дате
        await page.locator('#sms-to').fill('22.04.2022 15:00');         //Конечная дата
        await page.click('text=Обновить');
        await page.waitForLoadState();
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).not.toContainText('495');
        await expect(page.locator('#smses > div > div > div:nth-child(2) > div > div.tab-content > div > div > div > div > div')).not.toContainText('498');
    });
});
