const { test, expect } = require('@playwright/test');

test.describe('Тестирование вкладки партнеров', () => 
{
    test.beforeEach(async ({ page }) =>       //Вход в систему, выполняется до всех тестов
    {      
        test.setTimeout(50000);
        await page.goto('http://localhost:4002/login');       // https://sadmin-dev.webslot.co/login
        await page.fill('input[name="username"]', 'elizaveta');
        await page.fill('input[name="password"]', 'user');
        await page.click('#submit_btn');
        await page.waitForLoadState();
        await expect(page.locator('.content-header-title')).toHaveText('Elizaveta'); 
    });


    test('Тестирование вкладки Партнёров (в меню)', async ({page}) => {
        await page.click('text=Партнёры');
        await page.waitForLoadState();
        await expect(page.locator('body h2')).toHaveText('Партнёры'); 
        await page.locator('#search > input').fill('Elizaveta');
        await page.click('#search > div > button > i');
        await page.waitForLoadState();
        await expect(page.locator('body > div.robust-content.content.container-fluid > div > div.content-body.mt-2 > div > div.col-md-7')).toHaveText(/liza_zal1/);
        await page.click('#group-settings');    
        await page.waitForLoadState();
        await expect(page.locator('#group-settings-modal > div > div > div.modal-header > h4')).toHaveText('Настройки группы'); 

        await page.click('text=Оператор');
        await page.click('#tab3 > div > div > div:nth-child(2) > div > div > label:nth-child(3) > input[type=radio]');
        await page.click('#tab3 > div > div > div:nth-child(3) > div > div > label > div > span > input[type=checkbox]');       //1
        await page.click('#tab3 > div > div > div:nth-child(4) > div > div > label > div > span > input[type=checkbox]');       //2
        await page.click('#tab3 > div > div > div:nth-child(5) > div > div > label > div > span > input[type=checkbox]');       //3
        await page.click('#tab3 > div > div > div:nth-child(6) > div > div > label > div > span > input[type=checkbox]');       //4
        await page.click('#tab3 > div > div > div:nth-child(7) > div > div > label > div > span > input[type=checkbox]');       //5
        await page.click('text=Сохранить');
    });
});